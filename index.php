<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Sistem Informasi Sekolah</title>
        <link rel="stylesheet" href="assets/css/style.css">
    </head>
    <body>
        
        <div class="style-header">
            <h1>Halaman Login</h1>
        </div>

        <div class="style-form">
            <!-- HARUS DIHUBUNGKAN KE BE (BACK END) -->
            <form action="/sistem-informasi-sekolah/process-login.php" method="POST">

                <label>Email :</label>
                <br>
                <input type="text" id="f-email" name="emailx">
                <br>

                <label>Password :</label>
                <br>
                <input type="password" id="f-password" name="password">
                <br>

                <input type="submit" value="Submit">
            </form>
        </div>


        <script src="index.js"></script>
    </body>
</html>