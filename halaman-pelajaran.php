<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Halaman Pelajaran</title>
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <link rel="stylesheet" href="assets/css/brands.min.css">
        <link rel="stylesheet" href="assets/css/solid.min.css">
    </head>
    <body>
        
        <div class="left-menu"> 
            <ul>
                <li>
                    <a href="/sistem-informasi-sekolah/home.html">Menu Siswa</a>
                </li>
                <li>
                    <a href="/sistem-informasi-sekolah/halaman-guru.html">Menu Guru</a>
                </li>
                <li>
                    <a href="/sistem-informasi-sekolah/halaman-pelajaran.html">Menu Pelajaran</a>
                </li>
                <li>
                    <a href="#">Menu Beasiswa</a>
                </li>
            </ul>
        </div>

        <div class="style-profile">
            <div>
                <p class="y1">Wahyu Wiryana</p>
                <span class="fa-solid fa-user x1"></span>
            </div>
        </div>

        <div class="style-content">
            <div>
                <h1>Menu Pelajaran</h1>

                <table>
                    <tr>
                        <th>Kode</th>
                        <th>Nama matpel</th>
                    </th>
                    <tr>
                        <td>MTK</td>
                        <td>Matematika</td>
                    </tr>
                    <tr>
                        <td>BHI</td>
                        <td>Bahasa Indonesia</td>
                    </tr>
                </table>
            </div>
        </div>

    </body>
</html>