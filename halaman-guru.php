<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Halaman Guru</title>
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <link rel="stylesheet" href="assets/css/brands.min.css">
        <link rel="stylesheet" href="assets/css/solid.min.css">
    </head>
    <body>
        
        <div class="left-menu"> 
            <ul>
                <li>
                    <a href="/sistem-informasi-sekolah/home.html">Menu Siswa</a>
                </li>
                <li>Menu Guru</li>
                <li>Menu Pelajaran</li>
                <li>Menu Beasiswa</li>
            </ul>
        </div>

        <div class="style-profile">
            <div>
                <p class="y1">Wahyu Wiryana</p>
                <span class="fa-solid fa-user x1"></span>
            </div>
        </div>

        <div class="style-content">
            <div>
                <h1>Menu Guru</h1>

                <table>
                    <tr>
                        <th>NIK</th>
                        <th>Nama guru</th>
                        <th>Jabatan</th>
                    </th>
                    <tr>
                        <td>238974877892374</td>
                        <td>Deni Purnama</td>
                        <td>Guru Tetap</td>
                    </tr>
                    <tr>
                        <td>732487385788478</td>
                        <td>Edi Bagus</td>
                        <td>Guru Honorer</td>
                    </tr>
                </table>
            </div>
        </div>

    </body>
</html>